# D5 Kanban #

This repository contains code used to inform the design of Sixty North's
short *Deliver Domain-Driven Designs Dynamically* workshop (known as D5
for short), and our *Domain-Driven Design Patterns in Python* course.  The
workshop and course are products of *Sixty North* and this code (although 
not the course itself) is available under a permissive open-source license.

The code implements a simple domain model for a kanban board project management
tool - imagine something similar to Trello - which is backed by a super-simple
append-only event-store consisting of a text file containing a stream of JSON
structures.

## A course? ##

Yes. *Sixty North* offer a popular two-day classroom training course which
gives background on implementing
[Domain-Driven Design (DDD) Patterns in Python](http://sixty-north.com/domain_driven_design_in_python.html).


## How do I get set up? ###

You should be able to run the `application` package which contains a simple console
program which exercises the domain model and various infrastructure features:

    $ cd d5-kanban-python
    $ python -m application
    
There's also a partial RESTful HTTP web API implemented using `aiohttp` which can
be run with

    $ cd d5-kanban-python
    $ python -m webapi

