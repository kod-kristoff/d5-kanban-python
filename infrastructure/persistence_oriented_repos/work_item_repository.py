import datetime
import json
from pathlib import Path
from weakref import WeakValueDictionary

from kanban.domain.model import workitem
from kanban.domain.model.workitem import WorkItem
from utility.containers import universal_container
from utility.unique_id import UniqueId


class JsonWorkItemRepository(workitem.Repository):

    def __init__(self, store_directory):
        """Initialise a repository for WorkItems.

        Args:
            store_directory: The path to an existing directory which will be used
                for storing serialised objects.

        Raises:
            ValueError: If store_directory is not an existing directory.
            PermissionError: If store_directory exists but is not writeable.
        """
        store_path = Path(store_directory)
        if not store_path.exists():
            raise ValueError("Store path {} does not exist".format(store_path))
        if not store_path.is_dir():
            raise ValueError("Store path {} is not a directory".format(store_path))
        marker_path = store_path / '.json_work_item_repository'
        marker_path.touch(exist_ok=True)
        self._store_path = store_path
        self._identity_map = WeakValueDictionary()

    def put(self, work_item):
            """Updates an already stored work_item or adds an existing WorkItem.

            Args:
                work_item: The work_item to be stored.
            """
            data = self._json_dict_from_work_item(work_item)
            object_path = self._store_path / "{!s}.json".format(work_item.id)
            with object_path.open(mode='wt') as json_file:
                json.dump(data, json_file, ensure_ascii=False)
            self._identity_map[work_item.id] = work_item

    def work_item_with_id(self, work_item_id):
        """Retrieve a WorkItem by id.

        Args:
            work_item_id: An UniqueId corresponding to the WorkItem.

        Returns:
            The WorkItem.

        Raises:
            ValueError: If a WorkItem with the id could not be found.
        """
        try:
            return self._identity_map[work_item_id]
        except KeyError:
            object_path = self._store_path / "{!s}.json".format(work_item_id)
            if not object_path.exists():
                raise ValueError("No WorkItem with id {}".format(work_item_id))
            with object_path.open(mode='rt') as json_file:
                data = json.load(json_file)
            work_item = self._work_item_from_json_dict(data)
            self._identity_map[work_item.id] = work_item
            return work_item

    def work_items_with_ids(self, work_item_ids=None):
        """Retrieve multiple WorkItems by id.

        Args:
            work_item_ids: An optional iterable series of UniqueIds to
                which the results will be restricted. If None, all work items
                will be returned.

        Returns:
            An iterable series of WorkItems.
        """
        if work_item_ids is None:
            json_paths = self._store_path.glob('*.json')
            hexs = [path.stem for path in json_paths]
            work_item_ids = (UniqueId.from_hex(hex) for hex in hexs)

        for work_item_id in work_item_ids:
            yield self.work_item_with_id(work_item_id)

    def work_items_with_name(self, name, work_item_ids=None):
        """Retrieve WorkItems by name.

        Args:
            name (str): Work items with names equal to this value will
                be included in the result.

            work_item_ids: An optional iterable series of UniqueIds to
                which the results will be restricted. If None, all work items
                matching name will be returned.

        Returns:
            An iterable series of work items.
        """
        work_item_ids = set(work_item_ids) if work_item_ids is not None else universal_container()
        for object_path in self._store_path.glob('*.json'):
            with object_path.open(mode='rt') as json_file:
                data = json.load(json_file)
                work_item_id = UniqueId.from_hex(data['id'])
                if (data['name'] == name) and (work_item_id in work_item_ids):
                    work_item = self._work_item_from_json_dict(data)
                    self._identity_map[work_item.id] = work_item
                    yield work_item

    def _json_dict_from_work_item(self, work_item):
        data = {'id': str(work_item.id),
                'version': work_item.version,
                'name': work_item.name,
                'due_date': work_item.due_date if work_item.due_date is None else work_item.due_date.isoformat(),
                'content': work_item.content}
        return data

    def _work_item_from_json_dict(self, data):
        work_item = WorkItem(work_item_id=UniqueId.from_hex(data['id']),
                             work_item_version=data['version'],
                             name=data['name'],
                             due_date=data['due_date'] and datetime.datetime.strptime(data['due_date'], '%Y-%m-%d').date(),
                             content=data['content'])
        return work_item


