from infrastructure.event_sourced_repos.event_tracking_repository import EventTrackingRepository
from kanban.domain.model import board


class BoardRepository(board.Repository, EventTrackingRepository):
    """Concrete repository for Boards in terms of an event store.
    """

    def __init__(self, transient_event_queue, persistent_event_store, **kwargs):
        """Initialize.

        It is assumed that the concatenation of the event streams in persistent_event_store
        and transient_event_queue contain all events in which this repository has an interest.

        Args:
            transient_event_queue: An EventQueue from which unsaved and future events
                pertaining to aggregates which will be, or have been, put into this
                repository, can be retrieved. Essentially this is the collection from
                where "present" (happened, but unsaved) and future (yet to happen)
                events can be retrieved.

            persistent_event_store: An EventStore containing past events.

            **kwargs: Arguments to be forwarded to other base classes.
        """
        super().__init__(transient_event_queue=transient_event_queue,
                         persistent_event_store=persistent_event_store,
                         mutator=board.mutate,
                         **kwargs)

    def put(self, board):
        """Make the repository aware of an aggregate.

        This method is idempotent.

        Args:
            board: The aggregate to register with the repository.
        """
        self._register(board)

    def board_ids(self):
        """An iterable series of board IDs."""
        return self._extant_aggregate_ids()

    def board_with_id(self, board_id):
        """Retrieve a Board by ID.

        Args:
            board_id: An UniqueId corresponding to the Board.

        Returns:
            The Board.

        Raises:
            ValueError: If a Board with the id could not be found.
        """
        return self._aggregate_with_id(board_id)

    def boards_with_ids(self, board_ids=None):
        """Retrieve multiple Boards by id.

        Args:
            board_ids: An optional iterable series of UniqueIds to
                which the results will be restricted. If None, all boards
                will be returned.

        Returns:
            An iterable series of Boards.
        """
        return self._aggregates_with_ids(board_ids)

    def boards_with_name(self, name, board_ids=None):
        """Retrieve Boards by name.

        Args:
            name (str): Boards with names equal to this value will
                be included in the result.

            board_ids: An optional iterable series of UniqueIds to
                which the results will be restricted. If None, all boards
                matching name will be returned.

        Returns:
            An iterable series of Boards.
        """
        return filter(lambda board: board.name == name,
                      self.boards_with_ids(board_ids))

    def _aggregate_root_entity_class(self):
        return board.Board
