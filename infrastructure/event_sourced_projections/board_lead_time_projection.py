from itertools import chain

from infrastructure.event_player import replay_events
from kanban.domain.model import lead_time
from utility.itertools import consume


class LeadTimeProjection(lead_time.LeadTimeProjection):
    """Create a new LeadTimeProjection.

        Args:
            board_repository: An event-sourced BoardRepository.

            board_id: The id of the board for which to report lead times.
    """

    def __init__(self, board_repository, board_id):
        self._board_repository = board_repository
        super().__init__(board_id=board_id)

    def _load_events(self):
        """Initialize the projection with historical events."""

        events = chain(
                self._board_repository.persistent_event_store,
                filter(self._board_repository.is_from_tracked_aggregate,
                       self._board_repository.transient_event_queue))

        consume(
            replay_events(
                events=events,
                mutator=lead_time.mutate,
                aggregate_ids=[self._board_id],
                stream_primer=self,
            )
        )
